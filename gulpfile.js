var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix) {
    mix.sass('frontend/app.scss', 'public/dist/fe/styles/app.css');
    /*    mix.sass('frontend/app.scss', '../../code/frontend/app/app.css');*/
    mix.sass([
        'admin/app.scss'
    ], 'public/dist/admin/styles/app.css');
    mix.scripts([
            '../../../bower_components/dropzone/dist/dropzone.js',
            '../../../bower_components/geocomplete/jquery.geocomplete.js',
            '../../../bower_components/bootbox/bootbox.js',
            '../../../bower_components/toastr/toastr.js',
            '../../../bower_components/cropper/dist/cropper.js',
            '../../../bower_components/html.sortable/dist/html.sortable.js'],
        'public/dist/admin/js/vendor.js');
    mix.scriptsIn('resources/assets/js/admin', 'public/dist/admin/js/main.js');

});


