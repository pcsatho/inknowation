<?php

namespace App\Http\Controllers;

use App\Gallery;
use App\GalleryItem;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Intervention\Image\Facades\Image;
use SleepingOwl\Admin\Admin;

class GalleryController extends Controller {

	public function getAdminIndex()
	{
		$content = view('admin::gallery.index');
		$title   = 'Galéria';

		return Admin::view($content, $title);

	}


	public function update($item)
	{

		$id                = $item;
		$item              = Gallery::find($item);
		$input             = \Input::all();
		$item->title       = $input['title'];
		$item->description = $input['description'];
		$galleryItems      = array();
		$galleryImages     = array();

		foreach ($input['galleryItem'] as $index => $gal_item)
		{
			if ( ! isset( $gal_item['id'] ) )
			{
				$gal_item['gallery_id'] = $id;
				$gitem                  = new GalleryItem($gal_item);

				array_push($galleryItems, $gitem);

				array_push($galleryImages, $gal_item['image']);
			}
			else
			{

				$existing              = GalleryItem::find($gal_item['id']);
				$existing->title       = $gal_item['title'];
				$existing->description = $gal_item['description'];
				$existing->position = $gal_item['position'];
				$existing->cropX = $gal_item['cropX'] ? $gal_item['cropX'] : 0 ;
				$existing->cropY = $gal_item['cropY'] ? $gal_item['cropY'] : 0;


				Image::make($existing->getMedia()[0]->getPath())->resize(350,null, function ($constraint)
				{
					$constraint->aspectRatio();
				})->crop(350,200, 0, (int)$gal_item['cropY'])->save($existing->getMedia()[0]->getPath('small'));
				$existing->save();
			}


		}

		if ( $item->update() )
		{
			$items = $item->galleryItem()->saveMany($galleryItems);
			foreach ($items as $i => $saved)
			{

				$saved->addMedia($galleryImages[$i])->toMediaLibrary('gallery');
			}

			return redirect('/fomme_admin_06742/galleries');
		}


	}


}
