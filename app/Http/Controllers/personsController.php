<?php

namespace App\Http\Controllers;

use App\GalleryItem;
use Illuminate\Http\Request;


use App\Http\Requests;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use SleepingOwl\Admin\Admin;

class PersonsController extends Controller
{



    public function getAdminIndex()
    {
        $content = view('admin::persons.index');
        $title = 'Kik vagyunk?';
        return Admin::view($content, $title);

    }



}
