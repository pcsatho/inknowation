<?php

	namespace App\Http\Controllers;

	use App\Attendee;
	use Carbon\Carbon;
	use Webpatser\Uuid\Uuid;

	class ApiController extends Controller {

		public function getHome() {
			$data = \App\Home::all();
			foreach ($data as $i => $section)
			{
				if( $section->event_id ){
					$event = \App\Event::find($section->event_id);
					$data[$i]->event= $event;
				}
			}

			return \Response::json($data);
		}

		public function getGallery() {
			$data = \App\Gallery::with(array(
				'galleryItem' => function ($query)
				{
					$query->orderBy('position', 'asc');
				},'galleryItem.media' ))->orderBy('created_at','desc')->get();
			foreach ($data as $index => $gallery)
			{
					foreach($gallery->galleryItem as $i => $item){

						$data[$index]->galleryItem[$i]->small = $item->getMedia('gallery')[0]->getURL('small');

						$data[$index]->galleryItem[$i]->src = $item->getMedia('gallery')[0]->getURL();

						$data[$index]->galleryItem[$i]->width = getimagesize($item->getMedia('gallery')[0]->getPath())[0];
						$data[$index]->galleryItem[$i]->height = getimagesize($item->getMedia('gallery')[0]->getPath())[1];


					}
			}

			return \Response::json( $data );
		}

		public function getPosts() {
			$data = \App\Post::with([ 'tags', 'person' ] )->orderBy('created_at','desc')->get();


			return \Response::json( $data );
		}

		public function getPost( $id ) {
			$data = \App\Post::find( $id )->with([ 'tags','person' ])->first();

			return \Response::json( $data );
		}

		public function getEvents() {
			$data = \App\Event::with( 'program' )->orderBy('date_from','desc')->get();

			return \Response::json( $data );
		}

		public function getEvent($id) {
			$data = \App\Event::find( $id )->with( 'program' )->first();

			return \Response::json( $data );
		}


		public function postEvents()
		{


			$data = \Input::all()['data'];
			$a = new Attendee();
			$a->uuid = Uuid::generate()->string;
			$a->event_id = $data['event']['id'];
			$a->name= $data['name'];
			$a->title= $data['title'];
			$a->company_name= isset($data['company'])  ? $data['company'] : '';
			$a->tax_no= isset($data['taxNo']) ?$data['taxNo'] : '';
			$a->mailing_address= $data['mailAddress'];
			$a->billing_address= $data['invoiceAddress'];
			$a->phone= $data['phone'];
			$a->fax= isset($data['fax']) ? $data['fax'] :'';
			$a->email= $data['email'];
			$a->info= isset($data['comment']) ? $data['comment'] :'';
			$a->company_leader = isset($data['companyleader']) ? $data['companyleader'] : false ;
			if($a->save()){



				/*EMAIL ÍRÁSA a fom@fommefatal.hu-ra (configból kivenni)*/

				$context = array(
					'attendee' => $a
				);

				\Mail::send('email.attendee', $context, function ($message)
				{
					$message->from('rendszer@fommefatale.hu', 'FommeFatale.hu Rendszer üzenet');
					$message->to(\Settings::get('email'),'Fomme Fatale')->subject('Új jelentkező egy eseményre!');
				});

				\Mail::send('email.toAttendee', $context, function ($message) use ($a)
				{
					$message->from('rendszer@fommefatale.hu', 'FommeFatale.hu Rendszer üzenet');
					$message->to($a->email, $a->name)->subject('Jelentkezés a FOMme Fatale rendezvényére');
				});
				return \Response::json(array(
					'status'=>'success',
					'message'=>'Köszönjük jelentkezését!'
				),200);

			}else{
				return \Response::json(array(
					'status'  => 'error',
					'message' => 'Hiba történt, kérjük próbálja meg később!'
				), 200);
			}




		}


	/*	public function mailTest(){
			$a = Attendee::find(1);
			$context = array(
				'attendee' => $a
			);

			\Mail::send('email.toAttendee', $context, function ($message) use ($a)
			{
				$message->from('rendszer@fommefatale.hu', 'FommeFatale.hu Rendszer üzenet');
				$message->to($a->email, $a->name)->subject('Jelentkezés a FOMme Fatale rendezvényére');
			});
}*/

		public function getPrograms() {
			$data = \App\Program::with( 'events' )->get();

			return \Response::json( $data );
		}

		public function getProgram($id) {
			$data = \App\Program::find( $id )->with( 'events' )->first();

			return \Response::json( $data );
		}




		public function getPersons() {
			$data = \App\Person::all();

			return \Response::json( $data );
		}

		public function getPerson($slug) {
			$data = \App\Person::find( $slug )->first();

			return \Response::json( $data );
		}


		public function getPartners()
	{
		$data = \App\Partner::all();

		return \Response::json($data);
	}

		public function getKutatas()
		{
			$data = \App\Document::where('position','=','kutatas')->get();

			return \Response::json($data);
		}


		public function getPublikacio()
		{
			$data = \App\Document::where('position','=','publikacio')->get();

			return \Response::json($data);
		}



		public function getOptions()
		{
			$data = \Settings::getAll();

			return \Response::json($data);
		}


    }