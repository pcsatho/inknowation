<?php

namespace App\Http\Controllers;

use App\Gallery;
use App\GalleryItem;
use Illuminate\Http\Request;


use App\Http\Requests;
use App\Http\Controllers\Controller;


/**
 * Class GalleryItemController
 * @package App\Http\Controllers
 */
class GalleryItemController extends Controller
{

    public function upload()
    {


        $file = \Input::file('file');
        $destinationPath = public_path('images') . '/gallery';
        $filename = date('Ymdhis') . '-' . $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();
        $uploadSuccess = $file->move($destinationPath, $filename);
        $location = $destinationPath . "/" . $filename;


        if ($uploadSuccess) {

            $resp = array('location' => $location, 'public' => url('public/images/gallery/' . $filename));
            return \Response::json($resp, 200);
        } else {
            return \Response::json('error', 400);
        }
    }



    public function delete($id)
    {
        $galleryItem = GalleryItem::find($id);

        if($galleryItem->delete()){

            return \Response::json('' , 200);
        }
    }

}
