<?php
/**
 * Created by PhpStorm.
 * User: Peter
 * Date: 2015.09.25.
 * Time: 14:59
 */

namespace App\Http\Controllers;


use SleepingOwl\Admin\Admin;

class OptionsController extends Controller {



	public function getAdminIndex()
	{
		$content = view('admin::options.index');
		$title   = 'Általános beállítások';
	 \AssetManager::addScript(Admin::instance()->router->routeToAsset('ckeditor/ckeditor.js'));
	 \AssetManager::addScript(Admin::instance()->router->routeToAsset('ckeditor/config.js'));
		return Admin::view($content, $title);

	}


	public function postOptions()
	{
		$input = request()->except('_token');
		foreach($input as $option_key => $option_value){
			\Settings::set($option_key,$option_value);

		}

		return redirect()->back();

	}





}