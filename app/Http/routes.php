<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*Route::get('/{id}', ['as'=>'home','uses'=>'AttendeeController@getDetail']);*/
Route::group(array('prefix'=>'api'),function(){
    Route::get('home','ApiController@getHome');
    Route::get('gallery','ApiController@getGallery');
    Route::get('posts','ApiController@getPosts');
	Route::get('post/{id}','apiController@getPost' );
    Route::get('events','ApiController@getEvents');
    Route::post('events','ApiController@postEvents');
    Route::get('event/{id}','ApiController@getEvent');
	Route::get('programs','ApiController@getPrograms');
	Route::get('kutatas','ApiController@getKutatas');
	Route::get('publikacio','ApiController@getPublikacio');


	Route::get('program/{id}','ApiController@getProgram');
    Route::get('options', 'ApiController@getOptions');
	Route::get('persons','ApiController@getPersons');
	Route::get('person/{slug}','ApiController@getPerson');
    Route::get('partners','ApiController@getPartners');
});

/*Route::any('{path?}', function ()
{
    return File::get(public_path() . '/frontend/index.html');
})->where("path", ".+");*/

