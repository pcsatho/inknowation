<?php

	namespace App;

	use Cviebrock\EloquentSluggable\SluggableInterface;
	use Cviebrock\EloquentSluggable\SluggableTrait;
	use Illuminate\Database\Eloquent\Model;
	use SleepingOwl\Models\SleepingOwlModel;
	use SleepingOwl\Models\Interfaces\ModelWithImageFieldsInterface;
	use SleepingOwl\Models\Traits\ModelWithImageOrFileFieldsTrait;

	class Program extends SleepingOwlModel implements ModelWithImageFieldsInterface, SluggableInterface {
		use SluggableTrait;
		use ModelWithImageOrFileFieldsTrait;
		protected $guarded = [ 'id','imageConfirmDelete' ];


		protected $sluggable = [
			'build_from' => 'title',
			'save_to'    => 'slug',
		];


		public function events() {
			return $this->hasMany( 'App\Event' );
		}

		/*SleepingOwl Admin select generálás*/
		public static function getList() {
			return Program::all()->lists( 'title', 'id' )->toArray();
		}

		public function getImageFields() {
			return [
				'image' => 'programok/',
			];
		}
	}
