<?php


namespace App\Bug\Templates;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class GallerySmallCrop implements FilterInterface {

	public function applyFilter(Image $image)
	{

		return $image->fit(350);
	}
}