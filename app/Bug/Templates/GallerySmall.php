<?php


namespace App\Bug\Templates;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class GallerySmall implements FilterInterface {

	public function applyFilter(Image $image)
	{

		return $image->widen(350);
	}
}