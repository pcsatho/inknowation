<?php

namespace App\Composers;

use App\Http\Requests\Request;
use App\Post;
use Illuminate\Contracts\View\View;
use App\GalleryItem as GalleryItem;

class PostComposer
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    protected $post;

    public function __construct(Post $post)
    {
        // Dependencies automatically resolved by service container...
        $this->post = $post;
    }


    public function compose(View $view)
    {
        $id = \Route::current()->parameter('id');
            if($id){
                $post= Post::find($id);
                $view->with('post', $post);

            }

    }
}


