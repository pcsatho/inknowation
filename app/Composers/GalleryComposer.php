<?php

namespace App\Composers;

use App\Http\Requests\Request;
use Illuminate\Contracts\View\View;
use App\GalleryItem as GalleryItem;

class GalleryComposer
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    protected $galleryItems;


    public function __construct(GalleryItem $gallery)
    {
        // Dependencies automatically resolved by service container...
        $this->galleryItems = $gallery;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $id = \Route::current()->parameter('id');
            if($id){
                $galleryItems = GalleryItem::where('gallery_id','=',$id)->get()->sortBy('position');
                $view->with('galleryItems', $galleryItems);

            }

    }
}


