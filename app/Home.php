<?php

namespace App;

use SleepingOwl\Models\Interfaces\ModelWithImageFieldsInterface;
use SleepingOwl\Models\Interfaces\ModelWithOrderFieldInterface;
use SleepingOwl\Models\Traits\ModelWithOrderFieldTrait;
use SleepingOwl\Models\SleepingOwlModel;
use SleepingOwl\Models\Traits\ModelWithImageOrFileFieldsTrait;
use Illuminate\Database\Eloquent\Model;

class Home extends SleepingOwlModel implements ModelWithImageFieldsInterface, ModelWithOrderFieldInterface{

	use ModelWithImageOrFileFieldsTrait;
	use ModelWithOrderFieldTrait;

	protected $guarded = [ 'id' , 'imageConfirmDelete'];


	public function getSortField()
	{
		return 'position';
	}

	public function getImageFields()
	{
		return [
			'image' => 'home/',
		];
	}


}
