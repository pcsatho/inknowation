<?php
Admin::model(\App\Document::class)->title('Dokumentumok')

    ->columns(function ()
{

    Column::string('title', 'Dokumentum neve');
    Column::string('position', 'Pozíció');
    Column::string('created_at', 'Létrehozva');

})->form(function ()
{
    FormItem::text('title', 'Dokumentum Neve');
    FormItem::file('path', 'Dokumentum feltöltése');
    FormItem::select('position', 'Megjelenítés')->list(\App\Document::class);



});