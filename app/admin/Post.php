<?php

Admin::model(\App\Post::class)->title('Bejegyzések')->filters(function ()
{

})->columns(function ()
{
	Column::image('image')->sortable(false);
	Column::string('title', 'Cím');
	Column::date('created_at', 'Hozzáadva')->format('medium', 'short');
	/*Column::lists('tags.name', 'Tags');*/
})->form(function ()
{
	FormItem::text('title', 'Cím');
	FormItem::image('image', 'Bejegyzéshez tartozó kép');
	FormItem::view('admin::blog.imageCrop');
	FormItem::ckeditor('content', 'Bejegyzés szövege');
	FormItem::text('meta_title', 'Keresőkben látható cím');
	FormItem::textarea('meta_description', 'Keresőkben látható leírás');
/*    FormItem::multiSelect('tags', 'Címkék')->value('tags.tag_id')->list('App\Tag');*/
	FormItem::select('author','Szerző')->list('App\Person');
});