<?php
Admin::model(\App\Gallery::class)->title('Galériák')

    ->columns(function ()
{

    Column::string('title', 'Galéria neve');
    Column::string('created_at', 'Létrehozva');

})->form(function ()
{
    FormItem::text('title', 'Galéria Neve');
    FormItem::ckeditor('description', 'Leírás');
    FormItem::view('admin::gallery/index');

})->with('galleryItem')->async();