<?php

Admin::model(\App\Home::class)->title('Főoldal')->as('homepage')->filters(function ()
{

})->columns(function ()
{
	Column::image('image')->sortable(false);

	Column::string('title', 'Megnevezés');

	Column::date('created_at', 'Létrehozva')->format('medium', 'short');
})->form(function ()
{

	FormItem::text('title', 'Megnevezés');


	FormItem::image('image', 'Háttér');




});