<?php

/*
 * Describe you custom columns and form items here.
 *
 * There is some simple examples what you can use:
 *
 *		Column::register('customColumn', '\Foo\Bar\MyCustomColumn');
 *
 * 		FormItem::register('customElement', \Foo\Bar\MyCustomElement::class);
 *
 * 		FormItem::register('otherCustomElement', function (\Eloquent $model)
 * 		{
 *			AssetManager::addStyle(URL::asset('css/style-to-include-on-page-with-this-element.css'));
 *			AssetManager::addScript(URL::asset('js/script-to-include-on-page-with-this-element.js'));
 * 			if ($model->exists)
 * 			{
 * 				return 'My edit code.';
 * 			}
 * 			return 'My custom element code';
 * 		});
 */

Column::register('actionclass', App\Http\Controllers\ActionMoreController::class);
FormItem::register('imageCrop',App\Http\Controllers\ImageCropClass::class);



AdminRouter::get('attendees/show/{id}', 'App\Http\Controllers\AttendeeController@getDetail');
AdminRouter::post('galleries/upload/', 'App\Http\Controllers\GalleryItemController@upload');

AdminRouter::put('galleries/{id}/update/', 'App\Http\Controllers\GalleryController@update');
AdminRouter::post('gallery-item/delete/{id}', 'App\Http\Controllers\GalleryItemController@delete');

AdminRouter::post('options', 'App\Http\Controllers\OptionsController@postOptions');
	AdminRouter::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
