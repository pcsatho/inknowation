<?php

/*
 * Describe your menu here.
 *
 * There is some simple examples what you can use:
 *
 * 		Admin::menu()->url('/')->label('Start page')->icon('fa-dashboard')->uses('\AdminController@getIndex');
 * 		Admin::menu(User::class)->icon('fa-user');
 * 		Admin::menu()->label('Menu with subitems')->icon('fa-book')->items(function ()
 * 		{
 * 			Admin::menu(\Foo\Bar::class)->icon('fa-sitemap');
 * 			Admin::menu('\Foo\Baz')->label('Overwrite model title');
 * 			Admin::menu()->url('my-page')->label('My custom page')->uses('\MyController@getMyPage');
 * 		});
 */

Admin::menu(\App\Home::class);
Admin::menu(\App\Program::class);

Admin::menu(\App\Post::class);
Admin::menu(\App\Person::class);

Admin::menu(\App\Document::class);
Admin::menu()->url('galleries')->label('Galéria')->uses('App\Http\Controllers\GalleryController@getAdminIndex');
Admin::menu()->url('options')->label('Általános beállítások')->uses('App\Http\Controllers\OptionsController@getAdminIndex');
/*
Admin::menu(\App\Partner::class);
Admin::menu(\App\Event::class);
Admin::menu(\App\Attendee::class);*/
/*Admin::menu()->url('persons')->label('Kik vagyunk?')->uses('App\Http\Controllers\PersonsController@getAdminIndex');*/