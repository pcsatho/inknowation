<?php

Admin::model(App\Person::class)->title('Munkatársak')->columns(function ()
{
	Column::string('name', 'Name');

})->form(function ()
{

	FormItem::text('name', 'Név');
	FormItem::textarea('quote', 'Cím/idézet');
	FormItem::text('email', 'E-mail cím');
	FormItem::text('tel', 'Telefonszám');
	FormItem::ckeditor('body', 'Bővebb szöveg');
	FormItem::image('sign','Alárírás kép');
	FormItem::image('image','Profil Kép');
	FormItem::image('underImage','Alsó kép');

	FormItem::file('cv','Önéletrajz');

	FormItem::view('admin::person/index');

});

