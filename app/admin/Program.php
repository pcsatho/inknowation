<?php
Admin::model(\App\Program::class)->title('3F')->with('events')->filters(function ()
{
	ModelItem::filter('id')->title()->from(\App\Event::class);
})->columns(function ()
{
	Column::image('image')->sortable(false);
	Column::string('title', 'Program Neve');

})->form(function ()
{
	FormItem::text('title', 'Program Neve');
	FormItem::text('subtitle', 'Program Alcím');

	FormItem::ckeditor('body', 'Leírás');

});