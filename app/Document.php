<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Models\Interfaces\ModelWithFileFieldsInterface;
use SleepingOwl\Models\SleepingOwlModel;
use SleepingOwl\Models\Traits\ModelWithImageOrFileFieldsTrait;

class Document extends SleepingOwlModel implements ModelWithFileFieldsInterface{

use ModelWithImageOrFileFieldsTrait;

	public function getFileFields()
	{
		return [
				'path' => 'documents/',

		];
	}

        protected $table = 'documents';
        protected $fillable = ['title','path','position'];


	public static function getList()
	{

		return array(
			'kutatas'=>'Kutatások',
			'publikacio'=>'Publikációk'
		);
	}


}
