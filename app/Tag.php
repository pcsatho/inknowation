<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Tag extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $guarded = [];

    protected $sluggable = [
        'build_from' => 'name',
        'save_to'    => 'slug',
    ];

    public function posts()
    {
        return $this->belongsToMany('App\Post');
    }

    public static function getList(){

        return static::lists('name','id')->toArray();


    }
}
