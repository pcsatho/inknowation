<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;

class GalleryItem extends Model implements HasMediaConversions {

	use HasMediaTrait;

        protected $table = 'gallery_items';
        protected $fillable = ['title','gallery_id','position','description','cropX','cropY'];

	public function registerMediaConversions()
	{
		$this->addMediaConversion('thumb')->setManipulations([
				'w' => 300,
				'h' => 300,
				'fit'=>'crop'
			])->performOnCollections('gallery');



			$this->addMediaConversion('small')->setManipulations([ 'w' => 350,'h'=>'200', 'fit' => 'crop']);
			$this->addMediaConversion('big')->setManipulations([ 'w' => 1200 ]);


	}

	public function gallery()
	{
		return $this->belongsTo('App\Gallery');
	}


}
