<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Models\Interfaces\ModelWithImageFieldsInterface;
use SleepingOwl\Models\SleepingOwlModel;
use SleepingOwl\Models\Traits\ModelWithImageOrFileFieldsTrait;

class Partner extends SleepingOwlModel implements ModelWithImageFieldsInterface {

	use ModelWithImageOrFileFieldsTrait;

	protected $guarded = [ 'id' , 'personConfirmDelete','logoConfirmDelete'];


	public function getImageFields()
	{
		return [
			'logo'   => 'partnerek/',
			'person' => 'partnerek/',
		];

	}
}
