<?php

namespace App;
use Carbon\Carbon;
use SleepingOwl\Models\Interfaces\ModelWithImageFieldsInterface;
use SleepingOwl\Models\SleepingOwlModel;
use Illuminate\Database\Eloquent\Model;


class Event extends SleepingOwlModel implements ModelWithImageFieldsInterface
{


	public static function boot()
	{
		parent::boot();
/*
		static::updating(function($event){
		dd(\Input::all(),$event);
		});*/
	}
	protected $dates = [ 'created_at', 'updated_at','date_from','date_to' ];


	public function getImageFields()
	{
		return [

			'image'      => 'esemenyek/',

		];
	}

	protected $guarded = ['place','imageConfirmDelete'];
	public function program()
	{
		return $this->belongsTo('App\Program');
	}


	public function attendee()
	{
		return $this->hasMany('App\Attendee');
	}


	public function getDates()
	{
		return array_merge(parent::getDates(), [ 'date_from','date_to' ]);
	}


	public static function getList()
	{

		return \App\Event::all()->lists('title', 'id')->toArray();
	}


	public function getDateToAttribute($value)
	{
		if(!empty($value) && isset($value)){

		return Carbon::parse($value)->format('Y-m-d');
		}
	}




	public function getDateFromAttribute($value)
	{
		if ( !empty($value) && isset($value) ){

		return Carbon::parse($value)->format('Y-m-d');
		}
	}



	public function setDateFromAttribute($value)
	{


		$this->attributes['date_from'] = ($value == '' || $value== '0000-00-00') ? NULL : $value;
	}



	public function setDateToAttribute($value)
	{
		$this->attributes['date_to'] = ($value == '' || $value== '0000-00-00') ? NULL : $value;
	}


}
