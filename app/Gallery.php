<?php

namespace App;

use SleepingOwl\Models\SleepingOwlModel;
use Illuminate\Database\Eloquent\Model;


class Gallery extends SleepingOwlModel
{
    protected $fillable = ['title', 'description', 'galleryItem'];
    protected $table = 'galleries';
    public $galleryImages = [];


    public static function boot()
    {
        parent::boot();

        static::creating(function ($gallery) {
            \DB::beginTransaction();
            $gallery->setGalleryImages($gallery->galleryItem);
            unset($gallery->galleryItem);


        });
        static::created(function ($gallery) {
            foreach ($gallery->getGalleryImages() as $index=>$image) {


                $galleryItem = new GalleryItem;
                $galleryItem->title = $image['title'];
                $galleryItem->description = $image['description'];
                $galleryItem->position = $index;
                $galleryItem->gallery_id = $gallery->id;
                if ($galleryItem->save()) {
                    $galleryItem->addMedia($image['image'])->toCollection('gallery');
                    \DB::commit();
                } else {
                    \DB::rollBack();
                }
            }
        });

        static::updating(function ($gallery) {
            \DB::beginTransaction();
            $gallery->setGalleryImages($gallery->galleryItem);

            unset($gallery->galleryItem);


        });
        static::updated(function ($gallery) {

            foreach ($gallery->getGalleryImages() as $index=>$image) {

                if (!isset($image['id'])) {

                    $galleryItem = new GalleryItem;
                    $galleryItem->title = $image['title'];
                    $galleryItem->description = $image['description'];
                    $galleryItem->position = $index;
                    $galleryItem->gallery_id = $gallery->id;
                    if ($galleryItem->save()) {
                        $galleryItem->addMedia($image['image'])->toCollection('gallery');
                        \DB::commit();
                    } else {
                        \DB::rollBack();
                    }
                } else {
                    $galleryItem = GalleryItem::findOrNew($image['id']);

                    $galleryItem->title = $image['title'];
                    $galleryItem->description = $image['description'];
                    $galleryItem->position = $index;
                    $galleryItem->gallery_id = $gallery->id;
                    if ($galleryItem->save()) {

                        \DB::commit();
                    } else {
                        \DB::rollBack();
                    }

                }

            }
        });


    }


    public function galleryItem()
    {
        return $this->hasMany('App\GalleryItem');
    }

    private function setGalleryImages($images)
    {
        $this->galleryImages = $images;
    }

    private function getGalleryImages()
    {
        return $this->galleryImages;
    }





}
