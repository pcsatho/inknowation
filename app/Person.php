<?php

namespace App;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Models\Interfaces\ModelWithFileFieldsInterface;
use SleepingOwl\Models\Interfaces\ModelWithImageFieldsInterface;
use SleepingOwl\Models\SleepingOwlModel;



class Person extends SleepingOwlModel implements ModelWithImageFieldsInterface, ModelWithFileFieldsInterface,SluggableInterface {
	use SluggableTrait;
	protected $guarded = [ 'id','imageConfirmDelete','signConfirmDelete','underImageConfirmDelete' ];

	protected $table = "persons";

	protected $sluggable = [
		'build_from' => 'name',
		'save_to'    => 'slug',
	];

	public function getImageFields()
	{
		return [
			'sign' => 'persons/',
			'image' => 'persons/',
			'underImage' => 'persons/',
		];
	}

	public function getFileFields()
	{
		return [
			'cv' => 'persons/',
		];
	}


	public static function getList()
	{

		return static::lists('name', 'id')->toArray();


	}


	public function post(){
		return $this->hasOne('App\Post', 'author');
	}
}
