<?php

namespace App;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Image;
use SleepingOwl\Models\Interfaces\ModelWithImageFieldsInterface;
use SleepingOwl\Models\SleepingOwlModel;
use SleepingOwl\Models\Traits\ModelWithImageOrFileFieldsTrait;

class Post extends SleepingOwlModel implements SluggableInterface, ModelWithImageFieldsInterface {

	use SluggableTrait;
	use ModelWithImageOrFileFieldsTrait;

	public static function boot() {

		parent::boot();
		static::saving( function ( $post ) {
			if ( $post->id ) {
				if ( $post->image->exists() ) {
					$image = \Intervention\Image\Facades\Image::make( $post->image->getFullPath() );

					$oimage = $image->dirname . '/' . $image->filename . '-original.' . $image->extension;
					$oimage = \Intervention\Image\Facades\Image::make( $oimage );
					$oimage->resize( 700, null, function ( $constraint ) {
						$constraint->aspectRatio();
					} )->crop( 700, 250, 0, (int) $post->cropY )->save( $post->image->getFullPath() );
				}
			}
			unset( $post->cropY );
			unset( $post->oimage );
		} );

	}

	protected $guarded = [ 'id', 'cropX', 'imageConfirmDelete' ];

	protected $appends = array( 'oimage' );

	protected $attributes = array(
		'oimage' => '',
	);

	protected $sluggable = [
		'build_from' => 'title',
		'save_to'    => 'slug',
	];

	public function setTagsAttribute( $tags ) {
		$this->tags()->detach();
		if ( ! $tags ) {
			return;
		}
		if ( ! $this->exists ) {
			$this->save();
		}

		$this->tags()->attach( $tags );
	}

	public function getImageFields() {
		return [
			'image' => 'posts/',
		];
	}

	public function setImage( $field, $image ) {
		parent::setImage( $field, $image );
		$file = $this->$field;
		if ( ! $file->exists() ) {
			return;
		}
		$path       = $file->getFullPath();
		$image      = \Intervention\Image\Facades\Image::make( $path );
		$image_orig = \Intervention\Image\Facades\Image::make( $path );

		// you can use Intervention Image package features to change uploaded image
		$image_orig->resize( 700, null, function ( $constraint ) {
			$constraint->aspectRatio();
		} )->save( $image_orig->dirname . '/' . $image_orig->filename . '-original.' . $image_orig->extension );

		$image->resize( 700, null, function ( $constraint ) {
			$constraint->aspectRatio();
		} )->crop( 700, 250, 0, (int) $this->cropY )->save();


	}

	public function tags() {
		return $this->belongsToMany( 'App\Tag' );
	}

	public function person() {
		return $this->belongsTo( 'App\Person', 'author' );
	}

	public function getOimageAttribute() {

		if ( $this->image->exists() ) {
			$image = \Intervention\Image\Facades\Image::make( $this->image->getFullPath() );

			return $image->filename . '-original.' . $image->extension;
		}
	}

	public function scopeDefaultSort( $query ) {
		return $query->orderBy( 'created_at', 'desc' );
	}
}
