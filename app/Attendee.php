<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Models\SleepingOwlModel;

class Attendee extends SleepingOwlModel
{

	public function event()
	{
		return $this->belongsTo('App\Event');
	}


}
