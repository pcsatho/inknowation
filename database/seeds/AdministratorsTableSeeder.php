<?php

	use Illuminate\Database\Seeder;
	use SleepingOwl\AdminAuth\Entities\Administrator;

class AdministratorsTableSeeder extends Seeder
{
    public function run()
    {
	    Administrator::create([
		    'username' => 'admin',
		    'password' => 'SleepingOwl',
		    'name'     =>'Inknowation Adminisztrátor',
	    ]);
    }
}
