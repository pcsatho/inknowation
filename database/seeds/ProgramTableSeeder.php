<?php

use Illuminate\Database\Seeder;

class ProgramTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory('App\Program', 5)->create()->each(function ($u)
        {
            $u->events()->save(factory('App\Event')->make());
        });
    }
}
