f<?php

use Illuminate\Database\Seeder;

class PostTagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $postIDs = DB::table('posts')->lists('id');
        $tagIDs = DB::table('tags')->lists('id');

        $pivots = [];
        foreach($postIDs as $postID)
        {
            //necessary since shuffle() and array_shift() take an array by reference
            $randomtag = $tagIDs;

            shuffle($randomtag);
            for($index = 0; $index < rand(1,3); $index++) {
                $pivots[] = [
                    'post_id' => $postID,
                    'tag_id' => array_shift($randomtag)
                ];
            }
        }

        DB::table('post_tag')->insert($pivots);
    }
}
