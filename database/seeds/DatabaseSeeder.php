<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

         $this->call(ProgramTableSeeder::class);
         $this->call(AttendeesTableSeeder::class);
         $this->call(AdministratorsTableSeeder::class);
         $this->call(PostsTableSeeder::class);
         $this->call(TagsTableSeeder::class);
         $this->call(PostTagsTableSeeder::class);

         Model::reguard();
    }
}
