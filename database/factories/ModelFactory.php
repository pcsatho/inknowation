<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function ($faker)
{
	return [
		'name'           => $faker->name,
		'email'          => $faker->email,
		'password'       => str_random(10),
		'remember_token' => str_random(10),
	];
});

$factory->define(App\Program::class, function (Faker\Generator $faker)
{
	return [
		'title' => $faker->sentence,
		'image' => $faker->imageUrl(),
		'body'  => $faker->paragraph
	];
});

$factory->define(App\Event::class, function (Faker\Generator $faker)
{
	return [
		'program_id' => \App\Program::orderByRaw("RAND()")->first()->id,
		'title'      => $faker->sentence,
		'body'       => $faker->paragraph,
		'place_name' => $faker->city,
		'place_lat'  => $faker->latitude,
		'place_lng'  => $faker->longitude,
		'date_from'  => $faker->date(),
		'date_to'    => $faker->date(),
	];
});

$factory->define(App\Attendee::class, function (Faker\Generator $faker)
{
	return [
		'event_id' => \App\Event::orderByRaw("RAND()")->first()->id,
		'uuid' => $faker->uuid,
		'name' => $faker->name,
		'title'=> $faker->word,
		'company_name'=>$faker->company,
		'tax_no'=>$faker->swiftBicNumber,
		'mailing_address'=>$faker->address,
		'billing_address'=>$faker->address,
		'phone'=>$faker->phoneNumber,
		'fax'=>$faker->phoneNumber,
		'email'=>$faker->companyEmail,
		'info'=>$faker->paragraph(2),
		'company_leader'=>$faker->boolean()

	];
});


$factory->define(App\Post::class, function ($faker)
{
    $title = $faker->sentence;
    return [
        'title'=> $title,
        'meta_title'=> $title,
        'slug' => Illuminate\Support\Str::slug($title),
        'content'=> $faker->paragraph,
        'meta_description'=> $faker->paragraph
    ];
});


$factory->define(App\Tag::class, function ($faker)
{
    $word= $faker->word;
    return [
        'name'=> $word,
        'slug'=>\Illuminate\Support\Str::slug($word)

    ];
});
