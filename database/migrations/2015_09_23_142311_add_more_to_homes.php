<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreToHomes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('homes', function(Blueprint $table) {

            $table->string('event_id')->nullable();
            $table->text('event_teaser')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('homes', function(Blueprint $table) {
            $table->dropColumn(['event_id','event_teaser']);
        });
    }
}
