<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreToPrograms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('programs', function (Blueprint $table) {
		    $table->string('subtitle');
		    $table->text('quotes');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::table('programs', function (Blueprint $table) {
		    $table->dropColumn(['subtitle','quotes']);
	    });
    }
}
