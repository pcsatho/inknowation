<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttendeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attendees', function (Blueprint $table)
        {
            $table->increments('id');
            $table->string('uuid',32)->unique();
            $table->integer('event_id')->unsigned();
            $table->string('name');
            $table->string('title');
            $table->string('company_name');
            $table->string('tax_no');
            $table->string('mailing_address');
            $table->string('billing_address');
            $table->string('phone');
            $table->string('fax');
            $table->string('email');
            $table->text('info');
            $table->boolean('company_leader');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('attendees');
    }
}
