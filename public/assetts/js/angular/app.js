"use strict";
var app = angular.module('myApp', ['ngAnimate', 'smoothScroll', 'ui.router', 'multiStepForm', 'ngSanitize', 'ngProgress', 'ngPhotoSwipe', 'angularLazyImg', 'ngScrollbar']);

app.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', function ($stateProvider, $urlRouterProvider, $locationProvider) {
    $locationProvider.html5Mode(true);
    $urlRouterProvider.otherwise("/");
    $stateProvider

    // route for the home page
        .state('home', {
            url: '/', templateUrl: 'pages/home.html', controller: 'mainController'
        })

        // route for the about page
        .state('rolam', {
            url: '/rolam',


            templateUrl: 'pages/rolam.html', controller: 'whoamiController'


        })
        /*.state('kik-vagyunk.person', {
         parent: 'rolam', url: '/:slug', views: {
         'kik-vagyunk-person': {
         templateUrl: 'pages/kik-vagyunk.person.html', controller: 'personController'
         }
         }

         })
         */        // route for the contact page
        .state('tudasmenedzsment', {
            url: '/tudasmenedzsment', templateUrl: 'pages/tudasmenedzsment.html', controller: 'knowledgeController'

        })
        .state('kutatasok', {
            url: '/tudasmenedzsment/kutatasok',
            templateUrl: 'pages/tudasmenedzsment-kutatas.html',
            controller: 'knowledgeControllerKut'
        })
        .state('publikaciok', {
            url: '/tudasmenedzsment/publikaciok',
            templateUrl: 'pages/tudasmenedzsment-publikaciok.html',
            controller: 'knowledgeControllerPub'
        })


        .state('blog', {
            url: '/blog', templateUrl: 'pages/blog.html', controller: 'blogController'

        })

        .state('singlePost', {
            url: '/blog/:slug', templateUrl: 'pages/blog-single.html', controller: 'singlePostController'

        })


        /*

         .state('esemenyek', {
         url: '/esemenyek', templateUrl: 'pages/esemenyek.html', controller: "eventsController"

         }).state('esemenyek-single', {

         url: '/esemenyek/:id', templateUrl: 'pages/esemenyek.single.html', controller: 'singleEventController'


         })
         */


        .state('galeria', {
            url: '/galeria', templateUrl: 'pages/galeria.html', controller: 'galleryController'

        }).state('kapcsolat', {
        url: '/kapcsolat', templateUrl: 'pages/kapcsolat.html', controller: 'contactController'

    }).state('3f-inknowation', {
            url: '/3f-inknowation', templateUrl: 'pages/3f.html', controller: 'programController'

        })
        .state('3f-program', {

            url: '/3f-inknowation/:slug',
            templateUrl: 'pages/3f.program.html',
            controller: 'singleProgramController'


        })


}]);
app.run(['$rootScope', '$state', '$window', 'option', '$timeout', function ($rootScope, $state, $window, option, $timeout) {
    $rootScope.topBarClass = "";
    $rootScope.coloredLogo = true;
    $rootScope.goBack = function () {
        $window.history.back();
    };

    angular.element($window).bind("scroll", function () {
        if (this.pageYOffset == 0) {
            $rootScope.scrollTop = false;
        } else {
            $rootScope.scrollTop = true;
        }
        $rootScope.$apply();


    });

    option.getData().then(function (data) {
        $rootScope.options = data;
    });

    var init = function ($rootScope, next) {

        $rootScope.menuOpen = false;

        $rootScope.home = (next.name === 'home') ? true : false;


        switch (next.name) {
            case '3f-inknowation':
                $rootScope.bodyClass = 'inknowation';
                break;
            case '3f-program':
                $rootScope.bodyClass = 'in-program';
                break;
            default:
                $rootScope.bodyClass = next.name;
        }


        $rootScope.scrollTop = false;


    };


    init($rootScope, $state);
    $rootScope.$on("$stateChangeStart", function (event, next, current) {


        init($rootScope, next);
        angular.element(document).ready(function () {

            if (typeof  $('#fullpage').fullpage.destroy == 'function' && next.url != "/") {

                window.setTimeout(function () {

                    $('#fullpage').fullpage.destroy('all');

                }, 700);
            }
            //Angular breaks if this is done earlier than document ready.
        });
        if (next.name == "kik-vagyunk") {
            $timeout(function () {
                $rootScope.coloredLogo = false;
                console.log(next.name, 'kik-vagyunk init');

            }, 1000);

            $rootScope.topBarClass = "marsala";
        }
    });

    $rootScope.closeMenu = function ($event) {

        if ('menu-icon' != $event.target.parentNode.id && 'menu-icon' != $event.target.id) $rootScope.menuOpen = false;
    }
}]);
// create the controller and inject Angular's $scope


/*Home*/
app.factory('home', ['$http', function ($http) {

    var factory = {};
    factory.getData = function (callback) {

        return $http.get('/api/home', {cache: true}).then(function (data) {
            return data.data;
        });
    };

    factory.getTooltips = function () {
        return factory.getData().then(function (data) {
            var tooltips = [];

            for (var index = 0; index < data.length; ++index) {

                tooltips.push(data[index].title);
            }

            return tooltips;
        })
    };


    return factory;
}]);

app.controller('mainController', ['$scope', '$rootScope', '$http', 'home', 'event', function ($scope, $rootScope, $http, home, event) {
    $rootScope.isLoading = true;
    $rootScope.colored = false;

    home.getData().then(function (data) {
        $scope.datas = data;
        console.log(data);
        if (data.event_id) {
            event.getOne(event_id).then(function (data) {
                $scope.event_datas = data;
            });
        }
        $scope.$broadcast("homeContentLoaded");

    });


    $scope.message = 'Everyone come and see how good I look!';
    $rootScope.topBarClass = "nologo transparent";


}]);

/*Home vége*/


/*Kik Vagyunk*/
app.factory('persons', ['$http', function ($http) {
    var factory = {};
    factory.list = function (callback) {

        return $http.get('/api/persons', {cache: true}).then(function (data) {
            return data.data;
        });

    };
    factory.getOne = function (slug, callback) {

        return $http.get('/api/persons', {cache: true}).success(function (data) {
            var person = data.filter(function (entry) {
                return entry.slug === slug;
            })[0];
            callback(person);

        });

    };

    return factory;
}]);

app.controller('whoamiController', ['persons', '$scope', '$rootScope', '$timeout', function (persons, $scope, $rootScope, $timeout) {

    /*    $rootScope.topBarClass = "marsala";
     $rootScope.coloredLogo = false;*/

    $rootScope.seoTitle = 'Rólam :: Inknowation.hu';
    $rootScope.colored = false;


    persons.getOne('obermayer-nora', function (data) {
        $scope.person = data;

        $timeout(function () {
            $scope.loaded = true;

        }, 700)

    });

}]);

/*app.controller('personController', ['persons', '$rootScope', '$scope', '$stateParams', '$location', '$timeout', function (persons, $rootScope, $scope, $stateParams, $location, $timeout) {
 $rootScope.coloredLogo = true;
 $scope.slug = $stateParams.slug;
 $scope.path = $location.path();


 $rootScope.topBarClass = "white";
 $scope.active = function () {
 return true;
 };
 persons.getOne($stateParams.slug, function (data) {
 $scope.person = data;
 $rootScope.seoTitle = data.name + ':: FommeFatale.hu'
 $rootScope.seoImage = 'http://fommefatale.hu/public/images/persons/' + data.image;
 $timeout(function () {
 $scope.loaded = true;

 }, 700)

 });


 }]);*/

/*Kik vagyunk Vége*/


/*Programok*/
app.factory('programs', ['$http','$sce', function ($http,$sce) {
    var factory = {};
    factory.list = function (callback) {

        return $http.get('/api/programs', {cache: true}).then(function (data) {
            return data.data;
        });

    };
    factory.getOne = function (slug, callback) {

        return $http.get('/api/programs', {cache: true}).success(function (data) {
            var program = data.filter(function (entry) {
                return entry.slug === slug;
            })[0];

            program.body = $sce.trustAsHtml(program.body);

            callback(program);

        });

    };

    return factory;
}]);

app.controller('programController', ['$scope', '$rootScope', '$timeout','$sce', 'programs', function ($scope, $rootScope, $timeout,$sce, programs) {
    $rootScope.colored = true;
    $rootScope.seoTitle = '3F inKNOWation rendszer :: inknowation.hu';
    $rootScope.seoDescription = 'A szervezetek mindössze 37%-a rendelkezik tudásmenedzsment stratégiával, ugyanakkor 81%-a stratégiai eszköznek tekinti a tudást. . '

    jQuery('img.svg').each(function () {
        var $img = jQuery(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');


        var imgURL = $img.attr('src');

        jQuery.get(imgURL, function (data) {
            // Get the SVG tag, ignore the rest
            var $svg = jQuery(data).find('svg');

            // Add replaced image's ID to the new SVG
            if (typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }

            $svg = $svg.attr('ng-show', "coloredLogo");
            // Add replaced image's classes to the new SVG
            if (typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass + ' replaced-svg');
            }

            // Remove any invalid XML tags as per http://validator.w3.org
            $svg = $svg.removeAttr('xmlns:a');

            // Check if the viewport is set, if the viewport is not set the SVG wont't scale.
            if (!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
                $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
            }

            // Replace image with new SVG
            $img.replaceWith($svg);


            $scope = $svg.scope();
            $injector = $svg.injector();
            $injector.invoke(function ($compile) {
                $compile($svg)($scope)
            })

        }, 'xml');

    });

    programs.list().then(function (data) {
        $timeout(function () {
            $scope.programs = data;

        }, 700)
    });


}]);

app.controller('singleProgramController', ['programs', '$rootScope', '$scope', '$stateParams', '$location', '$timeout','$sce', function (programs, $rootScope, $scope, $stateParams, $location, $timeout,$sce) {

    $scope.slug = $stateParams.slug;
    $rootScope.coloredLogo = true;
    $scope.path = $location.path();
    $rootScope.topBarClass = "white";
    $rootScope.colored = false;

    programs.getOne($stateParams.slug, function (data) {
        $scope.program = data;
        $rootScope.seoTitle = data.title + ' :: inKNOWation.hu'
        $rootScope.seoImage = 'http://inknowation.hu/public/images/programok/' + data.image;
        $timeout(function () {
            $scope.loaded = true;

        }, 200)

    });


}]);
/*Programok vége*/



/*Partnerek*/

app.controller('knowledgeController', ['$scope', '$rootScope', '$http', '$timeout','option', function ($scope, $rootScope, $http, $timeout,option) {
    $rootScope.topBarClass = "white";
    $rootScope.colored = true;
    $rootScope.coloredLogo = true;
    $rootScope.seoTitle = 'Tudásmenedzsment :: inKNOWation.hu'

    option.getData().then(function (data) {
        $timeout(function () {
            $scope.option = data;
        }, 700);

    });
}]);





app.controller('knowledgeControllerPub', ['$scope', '$rootScope', '$http', 'publikacio', '$timeout','option', function ($scope, $rootScope, $http, publikacio, $timeout,option) {

    $rootScope.colored = false;

    $rootScope.seoTitle = 'Publikációk :: inKNOWation.hu';

    publikacio.getData().then(function (data) {
        $timeout(function () {
            $scope.documents = data;
        }, 700);

    });


    option.getData().then(function (data) {
        $timeout(function () {
            $scope.option = data;
        }, 700);

    });

}]);


app.controller('knowledgeControllerKut', ['$scope', '$rootScope', '$http', 'kutatas', '$timeout','option', function ($scope, $rootScope, $http, kutatas, $timeout,option) {
    $rootScope.colored = false;

    $rootScope.seoTitle = 'Kutatások :: inKNOWation.hu';

    kutatas.getData().then(function (data) {
        $timeout(function () {
            $scope.documents = data;
        }, 700);
        option.getData().then(function (data) {
            $timeout(function () {
                $scope.option = data;
            }, 700);

        });
    });



}]);


app.factory('kutatas', ['$http', function ($http) {

    var factory = {};
    factory.getData = function (callback) {

        return $http.get('/api/kutatas', {cache: true}).then(function (data) {
            return data.data;
        });
    };


    return factory;
}]);



app.factory('publikacio', ['$http', function ($http) {

    var factory = {};
    factory.getData = function (callback) {

        return $http.get('/api/publikacio', {cache: true}).then(function (data) {
            return data.data;
        });
    };


    return factory;
}]);



app.factory('partner', ['$http', function ($http) {

    var factory = {};
    factory.getData = function (callback) {

        return $http.get('/api/partners', {cache: true}).then(function (data) {
            return data.data;
        });
    };


    return factory;
}]);

/*Partnerek Vége*/


/*Blog*/

app.controller('blogController', ['$scope', '$rootScope', '$http', 'blog', '$timeout', function ($scope, $rootScope, $http, blog, $timeout) {
    $rootScope.topBarClass = "white";
    $rootScope.colored = true;

    $rootScope.seoTitle = 'Blog :: inKNOWation.hu';
    blog.getData().then(function (data) {
        $timeout(function () {
            $scope.posts = data;
        }, 700);

    });


}]);

app.controller('singlePostController', ['blog', '$rootScope', '$scope', '$stateParams', '$location', '$timeout', function (blog, $rootScope, $scope, $stateParams, $location, $timeout) {

    $scope.slug = $stateParams.slug;
    $rootScope.colored = false;

    $scope.path = $location.path();
    $rootScope.topBarClass = "white";

    blog.getOne($stateParams.slug, function (data) {
        $scope.post = data;
        $rootScope.seoTitle = data.title + ' :: inKNOWation.hu';
        $rootScope.seoImage = 'http://inknowation.hu/public/images/posts/' + data.image;
        $timeout(function () {
            $scope.loaded = true;

        }, 200)

    });


}]);


app.factory('blog', ['$http', '$sce', function ($http, $sce) {

    var factory = {};
    factory.getData = function (callback) {

        return $http.get('/api/posts', {cache: true}).then(function (data) {
            return data.data;
        });
    };

    factory.getOne = function (slug, callback) {

        return $http.get('/api/posts', {cache: true}).success(function (data) {
            var post = data.filter(function (entry) {

                return entry.slug === slug;
            })[0];
            post.content = $sce.trustAsHtml(post.content);
            callback(post);

        });

    };
    return factory;
}]);

/*Blog vége*/



/*Események*/

app.controller('eventsController', ['$scope', '$rootScope', '$http', 'event', function ($scope, $rootScope, $http, event) {
    $rootScope.topBarClass = "white";
    $rootScope.coloredLogo = true;
    $rootScope.seoTitle = 'Események :: inknowation.hu';
    event.getData().then(function (data) {

        $scope.datas = data;
        console.log(data);

    });


}]);

app.controller('singleEventController', ['event', '$rootScope', '$scope', '$stateParams', '$location', '$timeout', function (event, $rootScope, $scope, $stateParams, $location, $timeout) {

    $scope.id = $stateParams.id;
    $rootScope.coloredLogo = true;
    $scope.showModal = false;
    $scope.path = $location.path();
    $rootScope.topBarClass = "white";
    $scope.model = {};

    $scope.change = function () {

        if ($scope.model.companyleader) {
            $scope.model.event.price = $scope.model.event.price * 0.85;
        } else {
            $scope.model.event.price = $scope.model.event.price / 85 * 100;
        }
    }
    $scope.steps = [{
        hasForm: true,
        templateUrl: '/pages/form/step1.html',
        title: 'Esemény kiválasztása',
        controller: 'stepController'
    }, {
        templateUrl: '/pages/form/step2.html', hasForm: true, title: 'Személyes adatok megadása',
        controller: 'stepController'
    }, {
        templateUrl: '/pages/form/step3.html', hasForm: true, title: 'Elérhetőségek megadása',
        controller: 'stepController'
    }, {
        templateUrl: '/pages/form/step4.html', title: 'Összegzés', controller: 'stepController'
    },

    ];


    $scope.closeModal = function () {

        $scope.showModal = false;

    }

    $scope.finish = function () {
        event.postData($scope.model).then(function (response) {
            $scope.status = response.data.message;
            $scope.sent = true;
            $timeout(function () {
                $scope.showModal = false;
            }, 1000);
            if (response.data.status === "success") {

                $scope.alertClass = "success";

            } else {
                $scope.alertClass = "warning";
            }

        });
    }

    event.getData().then(function (data) {

        $scope.trainings = data;

    });


    event.getOne($stateParams.id, function (data) {
        $scope.event = data;
        $scope.model.event = data;
        $rootScope.seoTitle = data.title + ' esemény :: inknowation.hu';
        $rootScope.seoImage = 'http://inknowation.hu/public/images/esemenyek/' + data.image;

        $timeout(function () {
            $scope.loaded = true;


        }, 200)

    });


}]);

app.controller('stepController', ['$scope', 'multiStepFormInstance', '$timeout', function ($scope, multiStepFormInstance, $timeout) {
    var body = angular.element('.multi-step-body');

    $scope.$parent.onStepChange = function () {
        $timeout(function () {
            var step = angular.element('.form-step.ng-enter');

            if (step.length > 0) {
                body.animate({height: step.height()});
            }
        }, 200);

    }
}]);


app.factory('event', ['$http', function ($http) {

    var factory = {};
    factory.getData = function (callback) {

        return $http.get('/api/events', {cache: true}).then(function (data) {
            console.log(data);
            return data.data;
        });
    };

    factory.postData = function (model) {
        return $http.post('/api/events', {data: model}).then(function (response) {
            return response;
        }, function (response) {
            return response;
        });

    }

    factory.getOne = function (id, callback) {

        return $http.get('/api/events', {cache: true}).success(function (data) {
            var event = data.filter(function (entry) {

                return parseInt(entry.id) === parseInt(id);
            })[0];

            callback(event);

        });

    };


    return factory;
}]);

/*Események vége*/


/*Galéria*/
app.controller('galleryController', ['$scope', '$rootScope', 'gallery', function ($scope, $rootScope, gallery) {

    $rootScope.topBarClass = "white";
    $rootScope.seoTitle = 'Galéria :: inKNOWation.hu';
    $rootScope.colored = true;
    gallery.getData().then(function (data) {
        $scope.galleries = data;
        for (var g = 1; g <= $scope.galleries.length; g++) {
            $scope.galleries[g - 1].images = []
            for (var i = 1; i <= $scope.galleries[g - 1].gallery_item.length; i++) {

                $scope.galleries[g - 1].images.push({
                    src: $scope.galleries[g - 1].gallery_item[i - 1].src,
                    safeSrc: $scope.galleries[g - 1].gallery_item[i - 1].src,
                    thumb: $scope.galleries[g - 1].gallery_item[i - 1].small,
                    name: $scope.galleries[g - 1].gallery_item[i - 1].title,
                    size: screenSize($scope.galleries[g - 1].gallery_item[i - 1].width, $scope.galleries[g - 1].gallery_item[i - 1].height),
                    type: 'image'
                });
            }
        }
        $scope.loaded = true;
    });
    $scope.images = [];


    var screenSize = function (width, height) {

        var x = width ? width : $window.innerWidth;
        var y = height ? height : $window.innerHeight;

        return x + 'x' + y;
    };


}]);

app.factory('gallery', ['$http', function ($http) {

    var factory = {};
    factory.getData = function (callback) {

        return $http.get('/api/gallery', {cache: true}).then(function (data) {
            return data.data;
        });
    };
    /* factory.getOne = function (id, callback) {

     return $http.get('/api/events').success(function (data) {
     var event = data.filter(function (entry) {

     return parseInt(entry.id) === parseInt(id);
     })[0];

     callback(event);

     });

     };*/


    return factory;
}]);


/*Galéria Vége*/


/*Kapcsolat*/
app.controller('contactController', ['$scope', '$rootScope', function ($scope, $rootScope) {
    $rootScope.topBarClass = "white";
    $rootScope.colored = false;
    $rootScope.seoTitle = 'Kapcsolat :: inKNOWation.hu';

}]);
/*Kapcsolat Vége*/


app.factory('option', ['$http', function ($http) {

    var factory = {};
    factory.getData = function (callback) {

        return $http.get('/api/options', {cache: true}).then(function (data) {
            return data.data;
        });
    };
    return factory;
}]);


app.directive('loading', ['ngProgressFactory', function (ngProgressFactory) {
    return {
        restrict: 'E',
        replace: true,
        template: '<div id="splash" ng-show="isLoading">' +
        '<div class="outer"> ' +
        '<img class="img-responsive center-block" src="/dist/img/logo_white.svg" alt="inKNOWation"/>' +
        '<div class="cssload-loader"> <div class="cssload-side"></div> <div class="cssload-side"></div> <div class="cssload-side"></div> <div class="cssload-side"></div> <div class="cssload-side"></div> <div class="cssload-side"></div> <div class="cssload-side"></div> <div class="cssload-side"></div> </div>' +
        '</div> ' +
        '</div>',
        link: function (scope, element, attr) {
            /*       scope.progressbar = ngProgressFactory.createInstance();
             scope.progressbar.start();
             console.log(scope.progressbar)
             element.find('.filler').css('height',scope.progressbar.status());*/
        }
    }
}]);

app.directive('fullpage', ['$timeout', '$rootScope', 'home', function ($timeout, $rootScope, home) {
    return {
        restrict: 'AE',

        link: function (scope, el, atts) {

            home.getTooltips().then(function (data) {
                scope.tooltips = data;

            });
            scope.$on('homeContentLoaded', function () {


                if (typeof el.fullpage.destroy == 'function') {

                    el.fullpage.destroy('all');
                }

                $rootScope.isLoading = false;

                $timeout(function () {
                    el.fullpage({
                        //Navigation
                        menu: false,
                        lockAnchors: true,

                        navigation: false,
                        navigationPosition: 'right',
                        navigationTooltips: scope.tooltips,
                        showActiveTooltip: false,
                        slidesNavigation: true,
                        slidesNavPosition: 'bottom', //Scrolling
                        css3: true,
                        scrollingSpeed: 400,
                        autoScrolling: true,
                        fitToSection: true,
                        fitToSectionDelay: 1000,
                        scrollBar: false,
                        easing: 'easeInOutCubic',
                        easingcss3: 'cubic-bezier(.32,.55,.52,.96)',
                        loopBottom: true,
                        loopTop: false,
                        loopHorizontal: true,
                        continuousVertical: false,
                        normalScrollElements: '#element1, .element2',
                        scrollOverflow: true,
                        touchSensitivity: 15,
                        normalScrollElementTouchThreshold: 20, //Accessibility
                        keyboardScrolling: true,
                        animateAnchor: true,
                        recordHistory: false,

                        //Design
                        controlArrows: true,
                        verticalCentered: true,
                        resize: false,
                        sectionSelector: '.section',
                        paddingTop: '3em',
                        paddingBottom: '3em',
                        fixedElements: '#top-bar',
                        responsiveWidth: 0,
                        responsiveHeight: 0


                    });
                });


            });
            $('.down').click(function () {
                el.fullpage.moveSectionDown();
            });
        }
    }
}]);

app.directive('fullpageSection', [function () {
    return {
        restrict: 'AE', replace: true, templateUrl: 'templates/fullpage.html', scope: {
            sectiondata: '='
        }, /*
         * TODO
         *
         * A fulllpage hivatkozik a controllerbe.
         * itt elindítani egy broadcast eventet és a másikban akkor ellőni a fullpage initet ha az eventet elkaptuk.
         *
         * */
        link: function (scope, el, atts) {


        }
    }
}]);


app.filter('limitHtml', function () {
    return function (text, limit) {

        var changedString = String(text).replace(/<[^>]+>/gm, '');
        var length = changedString.length;

        return changedString.length > limit ? changedString.substr(0, limit - 1) + '... ' : changedString;
    }
});

app.filter('dateToISO', function () {
    return function (badTime) {
        var goodTime = badTime.replace(/(.+) (.+)/, "$1T$2Z");
        return goodTime;
    };
});

app.filter('breakFilter', function () {
    return function (text) {
        console.log(text);
        if (text !== undefined) return text.replace(/\n/g, '<br />');
    };
});