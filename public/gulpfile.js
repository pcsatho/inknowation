

var gulp = require('gulp');
var clean = require('gulp-clean');
var sass = require('gulp-ruby-sass');
var concat = require('gulp-concat');
var runSequence = require('run-sequence');
var minifyCss = require('gulp-minify-css');
var uglify = require('gulp-uglify');
var babel = require("gulp-babel");
var iconfont = require('gulp-iconfont');
var runTimestamp = Math.round(Date.now()/1000);
var consolidate = require('gulp-consolidate');
var rename = require("gulp-rename");
var connect = require('gulp-connect');


var fontName = "fommeFont";

//	===================================================
// 	Style specific Actions
//	===================================================
// Builds the style
gulp.task('buildStyles', function (callback) {

    runSequence(
        'sassStyles',
        'concatStyles',
        callback);

});
gulp.task('fontCustom', function () {

    return gulp.src(['assetts/icons/*.svg'])
        .pipe(iconfont({
            fontName: fontName, // required
            appendUnicode: false, // recommended option
            normalize:true,
            formats: ['ttf', 'eot', 'woff','woff2','svg'], // default, 'woff2' and 'svg' are available
            timestamp: runTimestamp // recommended to get consistent builds when watching files
        }))
        .on('glyphs', function(glyphs) {

            var options = {

                glyphs: glyphs.map(function(glyph) {
                    // this line is needed because gulp-iconfont has changed the api from 2.0

                    return { name: glyph.name, codepoint: glyph.unicode[0].charCodeAt(0) }
                }),
                fontName:fontName,
                fontPath: '../font/fomme/', // set path to font (from your CSS file if relative)
                className: 'fomme-icon' // set class name in your CSS
            };
            gulp.src('assetts/template/template.css')
                .pipe(consolidate('lodash', options))
                .pipe(rename({ basename:fontName }))
                .pipe(gulp.dest('dist/css/')); // set path to export your CSS



})
        .pipe(gulp.dest("dist/font/fomme/"));
});
//	Runs the SASS
gulp.task('sassStyles', function () {

    return sass('assetts/sass/style.scss')
        .on('error', function (err) {

            console.error('Error!', err.message);

        })
        .pipe(minifyCss({compatibility: 'ie8'}))
        .pipe( gulp.dest('dist/css') );	// Outputs to the root theme directory

});
// Concat the styles
gulp.task('concatStyles', function() {
    return gulp.src(['dist/css/header.css','dist/css/style.css'])
        .pipe(concat('style.css'))
        .pipe(gulp.dest('dist/css/'));
});

//	===================================================
// 	Minifies and uglifies the script
//	===================================================
// 	Builds the script
gulp.task('buildScripts', function(callback){
    runSequence(
        'uglifyScripts',
        callback);
});
// 	Uglifies the script
gulp.task('uglifyScripts', function(){
    return gulp.src([
        'bower_components/jquery/dist/jquery.js',


        'bower_components/photoswipe/dist/photoswipe-ui-default.js',
        'bower_components/photoswipe/dist/photoswipe.js',

        'bower_components/fullpage.js/vendors/jquery.slimscroll.js',
        'bower_components/fullpage.js/jquery.fullPage.min.js',


        'bower_components/angular/angular.js',
        'bower_components/angular-i18n/angular-locale_hu-hu.js',
        'bower_components/angular-animate/angular-animate.js',
        'bower_components/angular-sanitize/angular-sanitize.js',
        'bower_components/ngprogress/build/ngprogress.js',
        'bower_components/ng-scrollbar/dist/ng-scrollbar.js',
        'bower_components/angular-lazy-img/release/angular-lazy-img.js',


        'bower_components/angular-photoswipe/dist/angular-photoswipe.js',


        'bower_components/ngSmoothScroll/lib/angular-smooth-scroll.js',

        'bower_components/angular-ui-router/release/angular-ui-router.js',
        'bower_components/angular-multi-step-form/dist/browser/angular-multi-step-form.js',



        'assetts/js/app.js',

        'assetts/js/angular/*.js',
        'assetts/js/angular/*/*.js'

    ])
        .pipe(concat('script.min.js'))
      /*  .pipe(babel())*/
        .pipe(uglify())
        .pipe(gulp.dest('dist/js'));
});

//	===================================================
// 	Defaults
//	===================================================
// 	Default taks
gulp.task('default', function(callback) {

    runSequence(
        'buildStyles',
        'buildScripts',
        callback);

});


gulp.task('serve', function() {
    connect.server({
        port: 8080,
        host: '127.0.0.1',
        fallback: 'index.html'
    });
});

//	===================================================
// 	Runs all the watch scripts
//	===================================================
// Watches for the scss
gulp.watch(
	['assetts/sass/*.scss', 'assetts/sass/*/*.scss'], // Files to watch
	['buildStyles']	//Tasks to Run
);
// Watches for the scripts
gulp.watch(
    ['assetts/js/*/*.js'], // Files to watch
    ['buildScripts']	//Tasks to Run
);