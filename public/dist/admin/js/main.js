$.ajaxSetup({
	headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	}
});

jQuery(document).ready(function ($) {

    $(function () {

        $('#date_from,#date_to').datetimepicker({
            language: 'hu',
            format:'YYYY-MM-D',
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            }
        });
    });

	$(function () {


			$('#dataTable').on('click','.showAttendee', function (e) {
				showAttendee(e, this);
			});

		function showAttendee(e, elem) {
			e.stopPropagation();
			e.preventDefault();
			$(elem).find('i').removeClass('fa-eye').addClass('fa-spinner fa-spin');
			var attendeeID = $(elem).attr('data-id');
			console.log(attendeeID);
			var attendeeDetail = $.get('/fomme_admin_06742/attendees/show/' + attendeeID);

			attendeeDetail.success(function (data) {

				bootbox.alert(data);
			});
			attendeeDetail.fail(function () {
				alert("error");
			});
			attendeeDetail.always(function () {
				$(elem).find('i').removeClass('fa-spinner fa-spin').addClass('fa-eye');
			});


		}
	});
	$(function () {
		$('#place').geocomplete({
			map: "#map", details: '.form-group', detailsAttribute: "data-geo"
		});

		$.when($('#place').val($('#place_name').val())).then(function () {
			$('#place').trigger("geocode");
		});
	});


	$(function () {
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});

			var dzCont = '.galleries .dropzone';
		if ($(dzCont).length){
		Dropzone.autoDiscover = false;
		var dz = new Dropzone(dzCont, {url: "/fomme_admin_06742/galleries/upload"});

		dz.on("complete", function (file) {
			dz.removeFile(file);


		});

		dz.on("success", function (file, resp) {

			var count = parseInt($('.galleries .images .image').length),

				content = '<li class="image row">' + '  <i class="fa fa-times fa-2x deleteButton"></i>' + '  <div class="col-xs-12 col-md-1 text-center">' + ' <i class="fa fa-arrows-v fa-2x"></i>' + '  </div>' + ' <div class="col-xs-12 col-md-2">' + ' <img class="img-responsive center-block" src="' + resp.public + '" alt=""/>' + ' </div>' + '  <div class="col-xs-12 col-md-9">' + '<input type="hidden" name="galleryItem[' + count + 1 + '][image]" value="' + resp.location + '">' + '  <input type="text" value="" name="galleryItem[' + count + 1 + '][title]" placeholder="A kép neve">' + '  <textarea name="galleryItem[' + count + 1 + '][description]" cols="30" rows="7" placeholder="A kép leírása"></textarea>' + '<input name="galleryItem[' + count + 1 + '][position]" type="hidden" />' + ' </div>' + '</li>';
			$('.galleries .images').append(content);
			$('.images').sortable();
		});
		}
		$('.images').on('click', '.deleteButton', function () {

			var id = $(this).attr('data-id');
			var $this = $(this).parents('.image');

			if (id) {
				var xhr = $.post('/fomme_admin_06742/gallery-item/delete/' + id);
				xhr.success(function () {
					toastr.success('A kiválasztott elem sikeresen törölve!');
					$this.addClass('deleted').slideUp();
					setTimeout(function () {
						$this.remove();
					}, 1500)
				});
			} else {
				toastr.success('A kiválasztott elem sikeresen törölve!');
				$this.addClass('deleted').slideUp();
				setTimeout(function () {
					$this.remove();
				}, 1500)
			}
		})

	});
	$(function () {
		$('.images').sortable({
			handle: '.fa',
			forcePlaceholderSize: true,
			placeholder: '<div class="sortable-placeholder col-xs-12"><div></div></div>'
		});
	});
	$('.images').sortable().bind('sortupdate', function () {
		$(this).find('.image').each(function ($index) {

			$(this).find('.position').val($index);
		})
	});

	$(function () {
		var $image = $('#cropper > img'), cropBoxData={'left':'','top':'','width':350,'height':200} , canvasData,self,cropX,cropY;

		$('.cropModal').on('click',function(e){
			e.preventDefault();
			self = this;
			cropX = $(this).parent().find('.cropX');
			cropY = $(this).parent().find('.cropY');

			$('#cropper-modal').modal('show');
			$('#cropper > img').attr('src',$(this).attr('data-image'));

			cropBoxData.left = parseInt(cropX.val());
			cropBoxData.top = parseInt(cropY.val());

		});


		$('#cropper-modal').on('shown.bs.modal', function () {

			$image.cropper({
				movable: false, zoomable: false, rotatable: false, scalable: false, aspectRatio: 350 / 200,minCropBoxWidth:350,minCropBoxHeight:200,
				 built: function () {


					$image.cropper('setCropBoxData', cropBoxData);
					$image.cropper('setCanvasData', canvasData);
					 console.log($image.cropper('getCropBoxData'), $image.cropper('getCanvasData'));
				}
			});
		}).on('hidden.bs.modal', function () {
			cropBoxDat = $image.cropper('getCropBoxData');
			canvasDat = $image.cropper('getCanvasData');
			$image.cropper('destroy');

			cropX.val(cropBoxDat.left );
			cropY.val(cropBoxDat.top );
		});
	});


	$(function () {
		var $image = $('#cropper > img'), cropBoxDataPost = {
			'left': '',
			'top': '',
			'width': 700,
			'height': 200
		}, canvasData, self, cropX, cropY;

		$('.cropModal-post').on('click', function (e) {
			e.preventDefault();
			self = this;
			cropX = $(this).parent().find('#cropX');
			cropY = $(this).parent().find('#cropY');

			$('#cropper-modal-post').modal('show');
			$('#cropper > img').attr('src', $(this).attr('data-image'));



		});


		$('#cropper-modal-post').on('shown.bs.modal', function () {

			$image.cropper({
				movable: false,
				zoomable: false,
				rotatable: false,
				scalable: false,
				aspectRatio: 700 / 200,
				minCropBoxWidth: 700,
				minCropBoxHeight: 200,
				built: function () {


					$image.cropper('setCropBoxData', cropBoxDataPost);
					$image.cropper('setCanvasData', canvasData);
					console.log($image.cropper('getCropBoxData'), $image.cropper('getCanvasData'));
				}
			});
		}).on('hidden.bs.modal', function () {
			cropBoxDat = $image.cropper('getCropBoxData');
			canvasDat = $image.cropper('getCanvasData');
			$image.cropper('destroy');

			cropX.val(cropBoxDat.left);
			cropY.val(cropBoxDat.top);
		});
	});


});


//# sourceMappingURL=main.js.map
