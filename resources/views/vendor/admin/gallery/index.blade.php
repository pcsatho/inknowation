<div class="dropzone">
    <div class="dz-message" data-dz-message><span>Kattintson, vagy dobja ide a fájlokat.</span></div>

</div>


<ul class="images">
    @if(isset($galleryItems))
        @foreach($galleryItems as $index => $item)

            <li class="image row">
                <i data-id="{!!$item->id!!}" class="fa fa-times fa-2x deleteButton"></i>
                <div class="col-xs-12 col-md-1 text-center move">
                    <i class="fa fa-arrows-v fa-2x"></i>
                </div>

                <div class="col-xs-12 col-md-2">
                    <img class="img-responsive center-block" src="{!!asset($item->getMedia()[0]->getUrl('thumb'))!!}" alt=""/>
                    <button data-id="{!!$item->id!!}" data-image="/images/gallerysmall{!!$item->getMedia()[0]->getUrl()!!}" class="cropModal btn btn-block">Előnézeti kép szerkesztése</button>
                    <input class="cropX" type="hidden" name="galleryItem[{!!$item->id!!}][cropX]" value="{!! $item->cropX !!}">
                    <input class="cropY" type="hidden" name="galleryItem[{!!$item->id!!}][cropY]" value="{!! $item->cropY !!}">
                </div>
                <div class="col-xs-12 col-md-9">
                    <input type="hidden" name="galleryItem[{!!$item->id!!}][id]" value="{!!$item->id!!}">

                    <input type="text" value="{!!$item->title!!}" name="galleryItem[{!!$item->id!!}][title]"
                           placeholder="A kép neve">
                <textarea name="galleryItem[{!!$item->id!!}][description]" cols="30"
                          rows="7" placeholder="A kép leírása">{!!$item->description!!}</textarea>
                    <input class="position" name="galleryItem[{!!$item->id!!}][position]" type="hidden" value="{!!$index!!}"/>
                </div>
            </li>

        @endforeach


    @endif

</ul>


<div class="modal fade" id="cropper-modal">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-body">
                <div id="cropper">
                    <img src="" alt=""/>
                </div>
            </div>

        </div>
    </div>
</div>
