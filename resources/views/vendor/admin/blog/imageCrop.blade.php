@if(isset($post) && ($post->image->exists()))
<button data-id="{!!$post->id!!}" data-image="/images/featuredcrop/images/posts/{!!$post->oimage !!}"
        class="cropModal-post btn">Előnézeti kép szerkesztése
</button>

<input type="hidden" name="cropY" id="cropY"/>
<input type="hidden" name="cropX" id="cropX"/>

<div class="modal fade" id="cropper-modal-post">
        <div class="modal-dialog">
                <div class="modal-content">

                        <div class="modal-body">
                                <div id="cropper">
                                        <img src="" alt=""/>
                                </div>
                        </div>

                </div>
        </div>
</div>

@endif
