
<h1>Általános beállítások</h1>

{!! Former::vertical_open()->method('POST') !!}


<ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#contact-tab">Kapcsolati adatok</a></li>
    <li><a data-toggle="tab" href="#social-tab">Social adatok</a></li>
    <li><a data-toggle="tab" href="#page-tab">Oldal tartalmak</a></li>
</ul>

<div class="tab-content">
    <div id="contact-tab" class="tab-pane fade in active">
<h3>Kapcsolati adatok</h3>
        {!!Former::text('phone','Telefonszám')->value(Settings::get('phone'))!!}
        {!!Former::text('email','E-mail cím')->value(Settings::get('email'))!!}
        {!!Former::text('facebook','Facebook Link')->value(Settings::get('facebook'))!!}
        {!!Former::text('youtube','Youtube Link')->value(Settings::get('youtube'))!!}
        {!!Former::text('default_image','Alapértelmezett kép')->value(Settings::get('default_image'))!!}
    </div>
    <div id="social-tab" class="tab-pane fade">
<h3>Social adatok</h3>
        {!! Former::text('default_seo_title','Alapértelmezett SEO Cím')->value(Settings::get('default_seo_title')) !!}
        {!! Former::textarea('default_seo_description','Alapértelmezett SEO leírás')->value(Settings::get('default_seo_description')) !!}


    </div>
    <div id="page-tab" class="tab-pane fade">
        <h3>Oldal tartalmak</h3>
        {!! Former::textarea('kutatas_appends','Kutatások tartalom')->class('ckeditor')->value(Settings::get('kutatas_appends')) !!}
        {!! Former::textarea('publikaciok_appends','Publikációk tartalom')->class('ckeditor')->value(Settings::get('publikaciok_appends')) !!}
        {!! Former::textarea('tudasmenedzsment_appends','Tudásmenedzsment tartalom')->class('ckeditor')->value(Settings::get('tudasmenedzsment_appends')) !!}

    </div>
</div>

<hr/>

<hr>



{!!Former::actions( Button::primary('Mentés')->submit() ) !!}


{!!Former::close()!!}