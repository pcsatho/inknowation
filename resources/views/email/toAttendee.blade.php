<h2>Tisztelt {!! $attendee->name !!}</h2>

<b>Köszönjük, hogy jelentkezett programunkra!</b>
<p>Ne feledje, a jelentkezés lemondását csak írásban, postai úton jelezheti, legkésőb 48 órával a rendezvény előtt.
    <br/>
    Amennyiben a lemondás ezután érkezik meg, úgy a teljes részvételi díj kiszámlázzásra kerül.
</p>
<p>A lemondásban kérjük tüntesse fel nevét, és a következő azonosítót: <b>{!! $attendee->uuid !!}</b></p>
<br/>

Üdvözlettel, <br/>
FOMme Fatale