<ul class="attendee">

    <li><span>Név:</span> {{$attendee->name}} </li>
    <li><span>Titulus:</span>{{$attendee->title}} </li>
    <li><span>Cég neve:</span>{{$attendee->company_name}} </li>
    <li><span>Adószám:</span>{{$attendee->tax_no}} </li>
    <li><span>Levelezési cím:</span><br/>
        {{$attendee->mailing_address}}
    </li>
    <li><span>Számlázási cím</span><br/>
        {{$attendee->billing_address}}
    </li>
    <li><span>Telefon:</span>{{$attendee->phone}} </li>
    <li><span>Fax:</span>{{$attendee->fax or ''}} </li>
    <li><span>E-mail cím:</span>{{$attendee->email}} </li>
    <li><span>Egyéb Információ:</span><br/>
        {{$attendee->info}}
    </li>
    <li><span>Esemény neve:</span>{{$attendee->event->title}} </li>
    <li><span>Esemény kezdete:</span>{{$attendee->event->date_from}} </li>
    <li><span>Kkv vezető beosztás? </span>
        @if($attendee->company_leader)
            Igen
        @else
            Nem
        @endif

    </li>

    </li>


</ul>