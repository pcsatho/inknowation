<?php

return [
	'404'      => 'Az oldal nem található',
	'auth'     => [
		'title'          => 'Bejelentkezés',
		'username'       => 'Felhasználónév',
		'password'       => 'Jelszó',
		'login'          => 'Bejelentkezés',
		'logout'         => 'Kijelentkezés',
		'wrong-username' => 'Rossz felhasználónév',
		'wrong-password' => 'vagy jelszó'
	],
	'ckeditor' => [
		'upload'        => [
			'success' => 'Fájl feltöltve: \\n- Méret: :size kb \\n- Szélesség/magasság: :width x :height',
			'error'   => [
				'common'              => 'A fájl feltöltése sikertelen',
				'wrong_extension'     => 'A fájl ":file" kiterjesztése nem megfelelő',
				'filesize_limit'      => 'A maximum megengedett fájlméret: :size kb.',
				'imagesize_max_limit' => 'szélesség x magasság = :width x :height \\n A maximum szélesség x magasság : :maxwidth x :maxheight lehet',
				'imagesize_min_limit' => 'szélesség x magasság  = :width x :height \\n A minimum szélesség x magasság : :maxwidth x :maxheight kell, hogy legyen',
			]
		],
		'image_browser' => [
			'title'    => 'Kép beillesztése a szerverről',
			'subtitle' => 'Kép kiválasztása',
		],
	],
	'table'    => [
		'new-entry'      => 'Új hozzáadása',
		'edit'           => 'Szerkesztés',
		'delete'         => 'Törlés',
		'delete-confirm' => 'Biztos, hogy törölni akarja?',
		'delete-error'   => 'Hiba történt az elem törlése közben. Először a kapcsolódó elemeket kell törölni',
		'moveUp'         => 'Mozgatás fel',
		'moveDown'       => 'Mozgatás le',
		'filter'         => 'Hasonló elemek megjelenítése',
		'filter-goto'    => 'Megjelenítés',
		'save'           => 'Mentés',
		'cancel'         => 'Mégse',
		'download'       => 'Letöltés',
		'all'            => 'Összes',
		'processing'     => '<i class="fa fa-5x fa-circle-o-notch fa-spin"></i>',
		'loadingRecords' => 'Kérjük várjon...',
		'lengthMenu'     => '_MENU_ elem mutatása',
		'zeroRecords'    => 'Nem található egyező elem',
		'info'           => ' _START_ / _END_ összes: _TOTAL_ elem',
		'infoEmpty'      => '0 / 0 összes: 0 elem',
		'infoFiltered'   => '(szűrve _MAX_ elemből)',
		'infoThousands'  => '.',
		'infoPostFix'    => '',
		'search'         => 'keresés: ',
		'emptyTable'     => 'Nincs rendelkezésre álló adat',
		'paginate'       => [
			'first'    => 'Első',
			'previous' => '&larr;',
			'next'     => '&rarr;',
			'last'     => 'Utolsó'
		]
	],
	'select'   => [
		'nothing'  => 'Nincs semmi kijelölve',
		'selected' => 'selected'
	]
];